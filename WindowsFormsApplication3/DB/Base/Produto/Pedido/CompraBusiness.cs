﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication3.DB.Base.Produto.Pedido
{
    class CompraBusiness
    {
        public int Salvar(CompraDTO dto)
        {

            CompraDatabase db = new CompraDatabase();
            return db.Salvar(dto);
        }


        public List<CompraDTO> Listar()
        {
            CompraDatabase db = new CompraDatabase();
            return db.Listar();
        }
    }

}