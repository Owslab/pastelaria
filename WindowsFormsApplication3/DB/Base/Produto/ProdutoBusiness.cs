﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication3.DB.Base.Produto;

namespace WindowsFormsApplication3.DB.Base.Produto
{
    class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto)
        {           
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Salvar(dto);
        }    
        

        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();
        }
    }
   
}