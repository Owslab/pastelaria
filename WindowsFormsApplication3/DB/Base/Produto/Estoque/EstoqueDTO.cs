﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication3.DB.Base.Produto.Estoque
{
    class EstoqueDTO
    {
        public int Id { get; set; }
        public string Produto { get; set; }
        public decimal Preco { get; set; }
        public decimal Quantidade { get; set; }
    }
}
